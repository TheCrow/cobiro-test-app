import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { LoginService } from './login.service';
import { LoginData } from './login';
import { catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginFormComponent implements OnInit {
  loginForm: FormGroup;

  constructor(private loginService: LoginService) { }

  ngOnInit(): void {
    this.createFormGroup();
  }

  createFormGroup(): void {
    this.loginForm = new FormGroup({
      email: new FormControl(''),
      password: new FormControl('')
    });
  }

  submit(): void {
    console.log('Form submitted', this.loginForm.value);
    if (this.loginForm.invalid) {
      console.error('form is invalid');
      return;
    }

    const loginData: LoginData = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password
    };

    this.loginService.submitForm(loginData).pipe(
      catchError((err) => {
        console.error('Error occurred during posting login data', err);
        // here you can handle error locally and rethrow it
        return throwError(err);
      })
    ).subscribe(
      res => console.log('Success! HTTP response', res),
      err => console.error('HTTP Error', err),
        () => console.log('HTTP request completed')
    );

  }
}
