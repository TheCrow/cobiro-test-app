import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LoginData } from './login';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient) { }

  submitForm(loginData: LoginData) {
    console.log('Service submitForm', loginData);

    return this.http.post('https://hub.cobiro.com/v1/login', {
      data: {
        type: 'login',
        attributes: {
          email: loginData.email,
          password: loginData.password
        }
      }
    });
  }
}
